<?php

define('JDC_DIR_URI', get_template_directory_uri());
define('JDC_DIR_PATH', get_template_directory());
define('JDC_ASSETS_URI', JDC_DIR_URI.'/resources');

/**
 * Theme setup
**/
function jdc_theme_setup(){
    show_admin_bar(false);

    add_theme_support('title-tag');
    
    add_theme_support('post-thumbnails');

    add_theme_support( 'woocommerce');
    
    //set_post_thumbnail_size(1568, 9999);
    // add_image_size('small-thumbnail', '150', '100');

    add_theme_support('html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));
    
    // add_theme_support('custom-logo',array(
    //     'height'      => 190,
    //     'width'       => 190,
    //     'flex-width'  => false,
    //     'flex-height' => false
    // ));

    // add_theme_support('customize-selective-refresh-widgets');
    // add_theme_support('wp-block-styles');

    add_theme_support('align-wide');
    add_theme_support('editor-styles');
    add_editor_style('style-editor.css');

    register_nav_menus(array(
        'header-menu' => 'Header Menu',
        'footer-menu' => 'Footer Menu',
    ));

}
add_action('after_setup_theme', 'jdc_theme_setup');



/**
 * Enqeueue stylesheets and scripts
**/
function jdc_enqueue_scripts(){
    global $wp_version;
    global $template;

    // unneccessary scripts
    wp_deregister_script('wp-embed');
    wp_deregister_style('wp-block-library');
    

    wp_enqueue_style('font-montserrat', 'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap', [], $wp_version);

    wp_enqueue_style('font-source-sans-pro', 'https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap', [], $wp_version);

   
    // if(!is_admin()){
    //     wp_deregister_script('jquery');
    // }
    // wp_enqueue_script('jquery', JDC_ASSETS_URI.'/vendor/jquery-3.4.1.min.js', [], $wp_version);
    wp_enqueue_script('jquery');

    // Foundation JS
    wp_enqueue_script('foundation', JDC_ASSETS_URI.'/vendor/foundation/js/foundation.min.js', [], $wp_version);

    // Mmenu Light
    // wp_enqueue_style('mmenu-light', JDC_ASSETS_URI.'/vendor/mmenu/mmenu-light.css', [], $wp_version);
    // wp_enqueue_script('mmenu-light', JDC_ASSETS_URI.'/vendor/mmenu/mmenu-light.js', [], $wp_version);

    // slick
    wp_enqueue_style('slick', JDC_ASSETS_URI.'/vendor/slick/slick.css', [], $wp_version);
    wp_enqueue_script('slick', JDC_ASSETS_URI.'/vendor/slick/slick.min.js', [], $wp_version);
   
    // Masonry
    // wp_enqueue_script('masonry', JDC_ASSETS_URI.'/vendor/masonry.pkgd.min.js', [], $wp_version);

    // Match Height
    // wp_enqueue_script('match-height', JDC_ASSETS_URI.'/vendor/jquery.matchHeight-min.js', [], $wp_version);

    // AOS
    wp_enqueue_style('aos', JDC_ASSETS_URI.'/vendor/aos/aos.css', [], $wp_version);
    wp_enqueue_script('aos', JDC_ASSETS_URI.'/vendor/aos/aos.js', [], $wp_version);

    // Parallax
    // wp_enqueue_script('parallax', JDC_ASSETS_URI.'/vendor/parallax.min.js', [], $wp_version);

    // Fancybox
    // wp_enqueue_style('fancybox', JDC_ASSETS_URI.'/vendor/fancybox/jquery.fancybox.min.css', [], $wp_version);
    // wp_enqueue_script('fancybox', JDC_ASSETS_URI.'/vendor/fancybox/jquery.fancybox.min.js', [], $wp_version);

    // App
    wp_enqueue_style('app', JDC_ASSETS_URI.'/css/app.css', [], $wp_version);
    wp_enqueue_script('app', JDC_ASSETS_URI.'/js/app.js', [], $wp_version);
    

    // localize objects
    wp_localize_script('app', 'APP_OBJ', array(
        'ajax_url'  => admin_url('admin-ajax.php'),
        'home_url'  => home_url(),
        'theme_url' => JDC_DIR_URI,
        'nonce'     => wp_create_nonce('jdc-nonce')
    ));
}
add_action('wp_enqueue_scripts', 'jdc_enqueue_scripts', 20);


/**
 * remove wp emoji
**/
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');


/**
 * Register sidebars
**/
function jdc_register_sidebars(){
    // footer menus
    for($i=1;$i<=3;$i++){
        register_sidebar(array(
            'name'          => __('Footer Area '.$i),
            'id'            => 'footer-area-'.$i,
            'before_widget' => false,
            'after_widget'  => false,
            'before_title'  => false,
            'after_title'   => false,
        ));
    }
}
add_action('widgets_init', 'jdc_register_sidebars');


/**
 * Allow SVG
**/
function jdc_myme_types($mime_types){
    $mime_types['svg'] = 'image/svg+xml';
    return $mime_types;
}
add_filter('upload_mimes', 'jdc_myme_types');


/**
 * Activation Hook
**/
function jdc_activator(){
    require JDC_DIR_PATH.'/includes/activator.php';
    new JDC_Activator();
}
add_action('after_switch_theme', 'jdc_activator');



require JDC_DIR_PATH.'/includes/tgmpa/init.php';
require JDC_DIR_PATH.'/includes/post-types.php';
require JDC_DIR_PATH.'/includes/shortcodes.php';
require JDC_DIR_PATH.'/includes/utils.php';
require JDC_DIR_PATH.'/includes/public-hooks.php';
require JDC_DIR_PATH.'/includes/admin-hooks.php';
//require JDC_DIR_PATH.'/includes/options/theme-options.php';
require JDC_DIR_PATH.'/includes/options/home-options.php';
require JDC_DIR_PATH.'/includes/options/team-options.php';
