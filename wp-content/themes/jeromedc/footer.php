

        </main>
        <!-- /MAIN -->
        
        
        <!-- FOOTER -->
        <footer id="site-footer" class="site-footer">
            <div class="grid-container">
                <div class="grid-x grid-margin-x align-middle">
                    <div class="cell large-8 order-2">
                        <div data-smooth-scroll>
                            <?php wp_nav_menu(['theme_location' => 'header-menu', 'container' => false, 'menu_class' => 'menu align-right']) ?>
                        </div>
                        <!-- <ul class="menu align-right">
                            <li><a href="#">One</a></li>
                            <li><a href="#">Two</a></li>
                            <li><a href="#">Three</a></li>
                            <li><a href="#">Four</a></li>
                        </ul> -->
                    </div>
                    <div class="cell large-4 order-1">
                        <ul class="menu">
                            <li class="copyright menu-text">Copyright © 2021 Blenheims.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- /FOOTER -->

    </div>
    <!-- /WRAPPER -->
    

    <?php wp_footer() ?>

</body>
</html>