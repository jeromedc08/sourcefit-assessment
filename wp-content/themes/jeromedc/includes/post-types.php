<?php


function jdc_register_post_type(){

    // Block Management
    $labels = array(
        'name'                  => __('Block Managements'),
        'singular_name'         => __('Block Management'),
        'menu_name'             => __('Block Managements'),
        'name_admin_bar'        => __('Block Management'),
        'add_new'               => __('Add New'),
        'add_new_item'          => __('Add New Block Management'),
        'new_item'              => __('New Block Management'),
        'edit_item'             => __('Edit Block Management'),
        'view_item'             => __('View Block Management'),
        'all_items'             => __('Block Managements'),
        'search_items'          => __('Search Block Managements'),
        'parent_item_colon'     => __('Parent Block Managements:'),
        'not_found'             => __('No Block Managements found.'),
        'not_found_in_trash'    => __('No Block Managements found in Trash.')
    );

    $args = array(
        'labels'                => $labels,
        'description'           => 'lorem ipsum',
        'public'                => false,
        'publicly_queryable'    => false,
        'show_in_menu'          => true,
        'show_ui'               => true,
        'show_in_admin_bar'     => false,
        'show_in_nav_menus'     => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
        'has_archive'           => true,
        'hierarchical'          => false,
        'can_export'            => true,
        'menu_position'         => 5,
        'supports'              => array('title', 'editor', 'thumbnail'),
        'delete_with_user'      => true
    );

    register_post_type('block-management', $args);



    // Leaseholders & Residents
    $labels = array(
        'name'                  => __('Leaseholders & Residents'),
        'singular_name'         => __('Leaseholders & Residents'),
        'menu_name'             => __('Leaseholders & Residents'),
        'name_admin_bar'        => __('Leaseholders & Residents'),
        'add_new'               => __('Add New'),
        'add_new_item'          => __('Add New Leaseholders & Residents'),
        'new_item'              => __('New Leaseholders & Residents'),
        'edit_item'             => __('Edit Leaseholders & Residents'),
        'view_item'             => __('View Leaseholders & Residents'),
        'all_items'             => __('Leaseholders & Residents'),
        'search_items'          => __('Search Leaseholders & Residents'),
        'parent_item_colon'     => __('Parent Leaseholders & Residents:'),
        'not_found'             => __('No Leaseholders & Residents found.'),
        'not_found_in_trash'    => __('No Leaseholders & Residents found in Trash.')
    );

    $args = array(
        'labels'                => $labels,
        'description'           => 'lorem ipsum',
        'public'                => false,
        'publicly_queryable'    => false,
        'show_in_menu'          => true,
        'show_ui'               => true,
        'show_in_admin_bar'     => false,
        'show_in_nav_menus'     => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
        'has_archive'           => true,
        'hierarchical'          => false,
        'can_export'            => true,
        'menu_position'         => 5,
        'supports'              => array('title', 'editor', 'thumbnail'),
        'delete_with_user'      => true
    );

    register_post_type('leaseholders', $args);



    // Teams
    $labels = array(
        'name'                  => __('Teams'),
        'singular_name'         => __('Teams'),
        'menu_name'             => __('Teams'),
        'name_admin_bar'        => __('Teams'),
        'add_new'               => __('Add New'),
        'add_new_item'          => __('Add New Teams'),
        'new_item'              => __('New Teams'),
        'edit_item'             => __('Edit Teams'),
        'view_item'             => __('View Teams'),
        'all_items'             => __('Teams'),
        'search_items'          => __('Search Teams'),
        'parent_item_colon'     => __('Parent Teams:'),
        'not_found'             => __('No Teams found.'),
        'not_found_in_trash'    => __('No Teams found in Trash.')
    );

    $args = array(
        'labels'                => $labels,
        'description'           => 'lorem ipsum',
        'public'                => false,
        'publicly_queryable'    => false,
        'show_in_menu'          => true,
        'show_ui'               => true,
        'show_in_admin_bar'     => false,
        'show_in_nav_menus'     => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
        'has_archive'           => true,
        'hierarchical'          => false,
        'can_export'            => true,
        'menu_position'         => 5,
        'supports'              => array('title', 'editor', 'thumbnail'),
        'delete_with_user'      => true
    );

    register_post_type('team', $args);
}

add_action('init', 'jdc_register_post_type');