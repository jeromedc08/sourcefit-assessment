<?php

include_once JDC_DIR_PATH.'/includes/cmb2-init.php';

function jdc_register_team_options_metabox(){
    $prefix = 'jdc_';

	// Init CMB2
	$cmb2 = new_cmb2_box([
		'id'            => $prefix.'team_options',
		'title'         => esc_html__('Other Info', 'cmb2'),
		'object_types'  => ['team'],
		'vertical_tabs'	=> true
	]);


	/* FIELDS
	************************************************/
	$cmb2->add_field([
		'name'    => 'Job Title',
		'desc'    => '',
		'default' => '',
		'id'      => $prefix.'job_title',
		'type'    => 'text',
	]);
}
add_action('cmb2_admin_init', 'jdc_register_team_options_metabox');




