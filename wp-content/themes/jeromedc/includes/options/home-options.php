<?php

include_once JDC_DIR_PATH.'/includes/cmb2-init.php';

function jdc_register_home_options_metabox(){
	if(get_page_template_slug($_GET['post']) != 'templates/template-home.php')
		return;

    $prefix = 'jdc_';

	// Init CMB2
	$cmb2 = new_cmb2_box([
		'id'            => $prefix.'home_options',
		'title'         => esc_html__('Template Options', 'cmb2'),
		'object_types'  => ['page'],
		'vertical_tabs'	=> true,
		'tabs'			=> [
			[
				'id'	=> 'tab-1',
				'icon'	=> 'dashicons-media-text',
				'title'	=> 'Banner',
				'fields'	=> [
					$prefix.'hero_slider_background'
				]
			],
			[
				'id'	=> 'tab-2',
				'icon'	=> 'dashicons-media-text',
				'title'	=> 'Section 1',
				'fields'	=> [
					$prefix.'section_1_title',
					$prefix.'section_1_content',
					$prefix.'section_1_button_url',
					$prefix.'section_1_button_text',
				]
			],
			[
				'id'	=> 'tab-3',
				'icon'	=> 'dashicons-media-text',
				'title'	=> 'Section 2',
				'fields'	=> [
					$prefix.'section_2_title',
					$prefix.'section_2_content',
					$prefix.'section_2_block_management'
				]
			],
			[
				'id'	=> 'tab-4',
				'icon'	=> 'dashicons-media-text',
				'title'	=> 'Section 3',
				'fields'	=> [
					$prefix.'section_3_title',
					$prefix.'section_3_content',
					$prefix.'section_3_leaseholders'
				]
			],
			[
				'id'	=> 'tab-5',
				'icon'	=> 'dashicons-media-text',
				'title'	=> 'Section 4',
				'fields'	=> [
					$prefix.'section_4_title',
					$prefix.'section_4_content',
					$prefix.'section_4_featured_title',
					$prefix.'section_4_featured_image'
				]
			],
			[
				'id'	=> 'tab-6',
				'icon'	=> 'dashicons-media-text',
				'title'	=> 'Section 5',
				'fields'	=> [
					$prefix.'section_5_title',
					$prefix.'section_5_subtitle',
					$prefix.'section_5_content',
					$prefix.'contact_info',
				]
			],
			[
				'id'	=> 'tab-7',
				'icon'	=> 'dashicons-media-text',
				'title'	=> 'Section 6',
				'fields'	=> [
					$prefix.'section_6_title',
					$prefix.'section_6_subtitle',
					$prefix.'section_6_teams',
				]
			],
		]
	]);


	/* FIELDS
	************************************************/
	$cmb2->add_field([
		'name'    => 'Background Images',
		'desc'    => '',
		'id'      => $prefix.'hero_slider_background',
		'type'    => 'file_list',
		'options' => ['url' => false],
		'text'    => ['add_upload_file_text' => 'Add Image'],
		'query_args' => [
			'image/gif',
			'image/jpeg',
			'image/png',
		],
		'preview_size' => 'large'
	]);


	$cmb2->add_field([
		'name' => 'Title',
		'desc' => '',
		'type' => 'text',
		'id'   => $prefix.'section_1_title'
	]);

	$cmb2->add_field([
		'name' => 'Content',
		'desc' => '',
		'type' => 'wysiwyg',
		'id'   => $prefix.'section_1_content'
	]);

	$cmb2->add_field([
		'name' => 'Button URL',
		'desc' => '',
		'type' => 'text_url',
		'id'   => $prefix.'section_1_button_url'
	]);

	$cmb2->add_field([
		'name' => 'Button Text',
		'desc' => '',
		'type' => 'text_small',
		'id'   => $prefix.'section_1_button_text'
	]);




	$cmb2->add_field([
		'name' => 'Title',
		'desc' => '',
		'type' => 'text',
		'id'   => $prefix.'section_2_title'
	]);
	
	$blocks = get_posts('post_type=block-management');
	$blocks_items = [];
	foreach($blocks as $block){
		$blocks_items[$block->ID] = $block->post_title;
	}
	$cmb2->add_field([
		'name'		=> 'Select Block Management',
		'desc'		=> '',
		'id'		=> $prefix.'section_2_block_management',
		'type'		=> 'multicheck',
		'options'	=> $blocks_items,
	]);

	$cmb2->add_field([
		'name' => 'Content',
		'desc' => '',
		'type' => 'wysiwyg',
		'id'   => $prefix.'section_2_content'
	]);




	$cmb2->add_field([
		'name' => 'Title',
		'desc' => '',
		'type' => 'text',
		'id'   => $prefix.'section_3_title'
	]);
	
	$blocks = get_posts('post_type=leaseholders');
	$blocks_items = [];
	foreach($blocks as $block){
		$blocks_items[$block->ID] = $block->post_title;
	}
	$cmb2->add_field([
		'name'		=> 'Select Leaseholders & Residents',
		'desc'		=> '',
		'id'		=> $prefix.'section_3_leaseholders',
		'type'		=> 'multicheck',
		'options'	=> $blocks_items,
	]);

	$cmb2->add_field([
		'name' => 'Content',
		'desc' => '',
		'type' => 'wysiwyg',
		'id'   => $prefix.'section_3_content'
	]);




	$cmb2->add_field([
		'name' => 'Title',
		'desc' => '',
		'type' => 'text',
		'id'   => $prefix.'section_4_title'
	]);
	$cmb2->add_field([
		'name' => 'Content',
		'desc' => '',
		'type' => 'wysiwyg',
		'id'   => $prefix.'section_4_content'
	]);
	$cmb2->add_field([
		'name' => 'Featured Title',
		'desc' => '',
		'type' => 'text',
		'id'   => $prefix.'section_4_featured_title'
	]);
	$cmb2->add_field([
		'name'    => 'Featured Image',
		'desc'    => '',
		'id'      => $prefix.'section_4_featured_image',
		'type'    => 'file',
		'options' => ['url' => false],
		'text'    => ['add_upload_file_text' => 'Add Image'],
		'query_args' => [
			'image/gif',
			'image/jpeg',
			'image/png',
		],
		'preview_size' => 'large'
	]);




	$cmb2->add_field([
		'name' => 'Title',
		'desc' => '',
		'type' => 'text',
		'id'   => $prefix.'section_5_title'
	]);
	$cmb2->add_field([
		'name' => 'Sub Title',
		'desc' => '',
		'type' => 'text',
		'id'   => $prefix.'section_5_subtitle'
	]);
	$contact_info = $cmb2->add_field([
		'id'			=> $prefix.'contact_info',
		'title'			=> 'Contact Information',
		'type'			=> 'group',
		'options'		=> [
			'group_title'   => __( 'Contact Information', 'jdc' ) . ' {#}',
			'add_button'    => __( 'Add Contact Information', 'jdc' ),
			'remove_button' => __( 'Remove Contact Information', 'jd' ),
			'sortable'      => true,
		],
	]);
	$cmb2->add_group_field($contact_info, [
		'name'    => 'Name',
		'desc'    => '',
		'default' => '',
		'id'      => 'name',
		'type'    => 'text',
	]);
	$cmb2->add_group_field($contact_info, [
		'name'    => 'Address',
		'desc'    => '',
		'default' => '',
		'id'      => 'address',
		'type'    => 'text',
	]);
	$cmb2->add_group_field($contact_info, [
		'name'    => 'Phone',
		'desc'    => '',
		'default' => '',
		'id'      => 'phone',
		'type'    => 'text',
	]);
	$cmb2->add_group_field($contact_info, [
		'name'    => 'Email',
		'desc'    => '',
		'default' => '',
		'id'      => 'email',
		'type'    => 'text',
	]);

	


	

	$cmb2->add_field([
		'name' => 'Title',
		'desc' => '',
		'type' => 'text',
		'id'   => $prefix.'section_6_title'
	]);
	
	$blocks = get_posts('post_type=team');
	$blocks_items = [];
	foreach($blocks as $block){
		$blocks_items[$block->ID] = $block->post_title;
	}
	$cmb2->add_field([
		'name'		=> 'Select Teams',
		'desc'		=> '',
		'id'		=> $prefix.'section_6_teams',
		'type'		=> 'multicheck',
		'options'	=> $blocks_items,
	]);

	$cmb2->add_field([
		'name' => 'Sub Title',
		'desc' => '',
		'type' => 'text',
		'id'   => $prefix.'section_6_subtitle'
	]);
}
add_action('cmb2_admin_init', 'jdc_register_home_options_metabox');




