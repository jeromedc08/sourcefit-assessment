(function($){

    var app = {
        onReady: function(){
            app.stickyHeader();
        },	
        onLoad: function(){
            $(document).foundation();

            app.bannerSlider();
            app.bannerParallax();

            AOS.init({once: true, duration: 800});
		},
		utils: function(){
        },
        stickyHeader: function(){
            $(window).on('load scroll', function(){
                var scrolled = this.scrollY || this.pageYOffset;

                if(scrolled >= 100){
                    $('#site-header').addClass('sticky');
                }else{
                    $('#site-header').removeClass('sticky');
                }
            });
        },
        bannerSlider: function(){
            $('.banner-slider').slick({
                fade: true,
                autoplay: true
            });
        },
        bannerParallax: function(){
            $(window).on('scroll', function(){
                var scrolled = window.pageYOffset || window.scrollY;

                $('#banner').css('transform', 'translateY('+(scrolled/2)+'px)');
            });
        }
    }


    document.addEventListener('DOMContentLoaded', app.onReady);
    window.addEventListener('load', app.onLoad);

})(jQuery);
