<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php echo bloginfo('charset') ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <link rel="icon" type="image/png" href="<?php echo STR_ASSETS_URI ?>/img/favicon.png"> -->
    <!-- <title><?php bloginfo('title') ?></title> -->

    <?php wp_head() ?>

</head>
<body <?php body_class() ?>>

    <div class="preloader"></div>

    <!-- WRAPPER -->
    <div id="wrapper" class="wrapper">

        <!-- HEADER -->
        <header id="site-header" class="site-header">
            <div class="grid-container">
                <div class="flex-container align-middle">
                    <div class="flex-child-auto">
                        <ul class="menu">
                            <li>
                                <a href="<?php echo home_url() ?>" class="site-logo">BLENHEIMS</a>
                            </li>
                        </ul>
                    </div>
                    <div class="flex-child-shrink">
                        <div class="main-menu">
                            <a href="javascript:void(0)" class="menu-toggle">
                                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                            </a>
                            <div data-smooth-scroll>
                                <?php wp_nav_menu(['theme_location' => 'header-menu', 'container' => false, 'menu_class' => 'menu']) ?>
                            </div>
                            <!-- <ul class="menu">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About Stratisan</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul> -->
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- /HEADER -->


        <!-- MAIN -->
        <main id="site-main" class="site-main">