<?php

/* Template Name: Home Page */

the_post();

$post_id = get_the_ID();
$prefix = 'jdc_';

$banner_images = get_post_meta($post_id, $prefix.'hero_slider_background', true);

$section_1_title = get_post_meta($post_id, $prefix.'section_1_title', true);
$section_1_content = get_post_meta($post_id, $prefix.'section_1_content', true);
$section_1_button_url = get_post_meta($post_id, $prefix.'section_1_button_url', true);
$section_1_button_text = get_post_meta($post_id, $prefix.'section_1_button_text', true);

$section_2_title = get_post_meta($post_id, $prefix.'section_2_title', true);
$section_2_content = get_post_meta($post_id, $prefix.'section_2_content', true);
$section_2_items = get_post_meta($post_id, $prefix.'section_2_block_management', true);

$section_3_title = get_post_meta($post_id, $prefix.'section_3_title', true);
$section_3_content = get_post_meta($post_id, $prefix.'section_3_content', true);
$section_3_items = get_post_meta($post_id, $prefix.'section_3_leaseholders', true);

$section_4_title = get_post_meta($post_id, $prefix.'section_4_title', true);
$section_4_content = get_post_meta($post_id, $prefix.'section_4_content', true);
$section_4_featured_title = get_post_meta($post_id, $prefix.'section_4_featured_title', true);
$section_4_featured_image = get_post_meta($post_id, $prefix.'section_4_featured_image', true);

$section_5_title = get_post_meta($post_id, $prefix.'section_5_title', true);
$section_5_subtitle = get_post_meta($post_id, $prefix.'section_5_subtitle', true);
$section_5_contacts = get_post_meta($post_id, $prefix.'contact_info', true);

$section_6_title = get_post_meta($post_id, $prefix.'section_6_title', true);
$section_6_subtitle = get_post_meta($post_id, $prefix.'section_6_subtitle', true);
$section_6_items = get_post_meta($post_id, $prefix.'section_6_teams', true);

get_header(); ?>


<!-- BANNER -->
<section id="banner" class="sec-banner sec sec-top">
    <div class="grid-container fluid padding-0">
        <div class="grid-x grid-margin-x">
            <div class="cell large-12">
                <div class="banner-slider" data-aos="zoom-in">
                    <?php foreach($banner_images as $image): ?>
                    <div class="slide-item">
                        <figure>
                            <img src="<?php echo $image ?>" alt="">
                        </figure>
                        <div class="slide-content">
                            <div class="content-box">
                                <?php the_content() ?>
                            </div> 
                            <a href="#about" data-smooth-scroll class="button primary hollow button-cta">EXPLORE</a>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /BANNER -->


<!-- ABOUT -->
<section id="about" class="sec-about sec">
    <div class="grid-container">
        <div class="grid-x grid-margin-x align-center">
            <div class="cell large-10">
                <h2 class="sec-title text-center" data-aos="zoom-in"><span><?php echo $section_1_title ?></span></h2>
                <div class="sec-content" data-aos="zoom-in">
                    <div class="text-wrap text-center">
                        <?php echo wpautop($section_1_content) ?>
                    </div>
                    <div class="button-wrap text-right">
                        <a href="<?php echo $section_1_button_url ?>" class="button primary button-arrow"><?php echo $section_1_button_text ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /ABOUT -->


<section id="block-management" class="sec-block sec">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <h2 class="sec-title text-center" data-aos="zoom-in"><span><?php echo $section_2_title ?></span></h2>
            </div>
        </div>
        <div class="grid-x grid-margin-x">
            <?php $items = get_posts(['post_type' => 'block-management', 'post__in' => $section_2_items, 'order' => 'asc']) ?>

            <?php foreach($items as $item): ?>
            <div class="cell large-3">
                <figure class="text-image" data-aos="zoom-in">
                    <?php echo get_the_post_thumbnail($item->ID) ?>
                    <figcaption><?php echo $item->post_title ?></figcaption>
                </figure>
            </div>
            <?php endforeach ?>

            <!-- <div class="cell large-3">
                <figure class="text-image" data-aos="zoom-in">
                    <img src="<?php echo JDC_ASSETS_URI ?>/img/thumb-1.jpg" alt="">
                    <figcaption>Lorem ipsum dolor sit amet</figcaption>
                </figure>
            </div>
            <div class="cell large-3">
                <figure class="text-image" data-aos="zoom-in">
                    <img src="<?php echo JDC_ASSETS_URI ?>/img/thumb-1.jpg" alt="">
                    <figcaption>Lorem ipsum dolor sit amet</figcaption>
                </figure>
            </div>
            <div class="cell large-3">
                <figure class="text-image" data-aos="zoom-in">
                    <img src="<?php echo JDC_ASSETS_URI ?>/img/thumb-1.jpg" alt="">
                    <figcaption>Lorem ipsum dolor sit amet</figcaption>
                </figure class="text-image">
            </div>
            <div class="cell large-3">
                <figure class="text-image" data-aos="zoom-in">
                    <img src="<?php echo JDC_ASSETS_URI ?>/img/thumb-1.jpg" alt="">
                    <figcaption>Lorem ipsum dolor sit amet</figcaption>
                </figure>
            </div> -->
        </div>
        <div class="grid-x grid-margin-x align-center">
            <div class="cell large-8">
                <div class="text-wrap" data-aos="zoom-in">
                    <?php echo wpautop($section_2_content) ?>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="leaseholders-and-residents" class="sec-lease sec">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <h2 class="sec-title text-center" data-aos="zoom-in"><span><?php echo $section_3_title ?></span></h2>
            </div>
        </div>
        <div class="grid-x grid-margin-x align-center">
            <?php $items = get_posts(['post_type' => 'leaseholders', 'post__in' => $section_3_items, 'order' => 'asc']) ?>

            <?php foreach($items as $item): ?>
            <div class="cell large-3">
                <figure class="text-image" data-aos="zoom-in">
                    <?php echo get_the_post_thumbnail($item->ID) ?>
                    <figcaption><?php echo $item->post_title ?></figcaption>
                </figure>
            </div>
            <?php endforeach ?>
            <!-- <div class="cell large-3">
                <figure class="text-image" data-aos="zoom-in">
                    <img src="<?php echo JDC_ASSETS_URI ?>/img/thumb-1.jpg" alt="">
                    <figcaption>Lorem ipsum dolor sit amet</figcaption>
                </figure>
            </div>
            <div class="cell large-3">
                <figure class="text-image" data-aos="zoom-in">
                    <img src="<?php echo JDC_ASSETS_URI ?>/img/thumb-1.jpg" alt="">
                    <figcaption>Lorem ipsum dolor sit amet</figcaption>
                </figure>
            </div> -->
        </div>
        <div class="grid-x grid-margin-x align-center">
            <div class="cell large-8">
                <div class="text-wrap" data-aos="zoom-in">
                    <?php echo wpautop($section_3_content) ?>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="flat-management" class="sec-flat sec">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <h2 class="sec-title text-center" data-aos="zoom-in"><span><?php echo $section_4_title ?></span></h2>
                
                <div class="sec-content text-center" data-aos="zoom-in">
                    <div class="text-wrap">
                    <?php echo wpautop($section_4_content) ?>
                    </div>

                    <figure class="title-image" data-aos="zoom-in">
                        <figcaption><?php echo $section_4_featured_title ?></figcaption>
                        <img src="<?php echo $section_4_featured_image ?>" alt="">
                        
                    </figure>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="contact" class="sec-contact sec">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <h2 class="sec-title text-center" data-aos="zoom-in"><span><?php echo $section_5_title ?></span></h2>
                <h3 class="sec-subtitle" data-aos="zoom-in"><?php echo $section_5_subtitle ?></h3>
            </div>
            <?php foreach($section_5_contacts as $contact): ?>
            <div class="cell large-4">
                <div class="contact-info" data-aos="zoom-in">
                    <h4><span class="tc-secondary"><?php echo $contact['name'] ?></span></h4>
                    <p><?php echo $contact['address'] ?></p>
                    <p><strong>Phone:</strong> <a href="tel:<?php echo $contact['phone'] ?>"><?php echo $contact['phone'] ?></a></p>
                    <p><strong>Email:</strong> <a href="mailto:<?php echo $contact['email'] ?>"><?php echo $contact['email'] ?></a></p>
                </div>
            </div>
            <?php endforeach ?>
            <!-- <div class="cell large-4">
                <div class="contact-info" data-aos="zoom-in">
                    <h4><span class="tc-secondary">Alabama</span></h4>
                    <p>4909 Lonely Oak Drive<br>
                    Mobile, AL 36582</p>
                    <p><strong>Phone:</strong> 000-000-0000</p>
                    <p><strong>Email:</strong> <a href="mailto:Blenheims@blenheims.com">Blenheims@blenheims.com</a></p>
                </div>
            </div>
            <div class="cell large-4">
                <div class="contact-info" data-aos="zoom-in">
                    <h4><span class="tc-secondary">South Carolina</span></h4>
                    <p>982 Wexford Way<br>
                    North Augusta, SC 29841</p>
                    <p><strong>Phone:</strong> 000-000-0000</p>
                    <p><strong>Email:</strong> <a href="mailto:Blenheims@blenheims.com">Blenheims@blenheims.com</a></p>
                </div>
            </div>
            <div class="cell large-4">
                <div class="contact-info" data-aos="zoom-in">
                    <h4><span class="tc-secondary">California</span></h4>
                    <p>4188 Black Oak Hollow Road<br>
                    Milpitas, CA 95035</p>
                    <p><strong>Phone:</strong> 000-000-0000</p>
                    <p><strong>Email:</strong> <a href="mailto:Blenheims@blenheims.com">Blenheims@blenheims.com</a></p>
                </div>
            </div> -->
        </div>
    </div>
</section>


<section id="team" class="sec-team sec">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <h2 class="sec-title text-center" data-aos="zoom-in"><span><?php echo $section_6_title ?></span></h2>
                <h3 class="sec-subtitle" data-aos="zoom-in"><?php echo $section_6_subtitle ?></h3>
            </div>
            <div class="cell">
                <?php $items = get_posts(['post_type' => 'team', 'post__in' => $section_6_items, 'order' => 'asc']) ?>

                <?php foreach($items as $item): ?>
                <div class="media-object person-block" data-aos="zoom-in">
                    <div class="media-object-section thumb">
                        <?php echo get_the_post_thumbnail($item->ID) ?>
                    </div>
                    <div class="media-object-section text">
                        <h4 class="title"><?php echo $item->post_title ?></h4>
                        <h5 class="subtitle"><?php echo get_post_meta($item->ID, $prefix.'job_title', true) ?></h5>
                        <div class="description">
                            <?php echo wpautop($item->post_content) ?>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
                <!-- <div class="media-object person-block" data-aos="zoom-in">
                    <div class="media-object-section thumb">
                        <img src="<?php echo JDC_ASSETS_URI ?>/img/person-2.jpg" alt="">
                    </div>
                    <div class="media-object-section text">
                        <h4 class="title">Levi Ackerman</h4>
                        <h5 class="subtitle">Managing Director</h5>
                        <div class="description">
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempore ullam fugit soluta ipsam! Quod molestias saepe facilis dicta, quis mollitia, corporis ex est, debitis voluptates eaque inventore minus modi? Perspiciatis eligendi, vel necessitatibus, quia quibusdam modi incidunt distinctio ratione ea omnis dolorum labore. Quisquam nam blanditiis eligendi saepe fugit praesentium.</p>
                        </div>
                    </div>
                </div>
                <div class="media-object person-block" data-aos="zoom-in">
                    <div class="media-object-section thumb">
                        <img src="<?php echo JDC_ASSETS_URI ?>/img/person-3.jpg" alt="">
                    </div>
                    <div class="media-object-section text">
                        <h4 class="title">Erwin Smith</h4>
                        <h5 class="subtitle">Technical Manager</h5>
                        <div class="description">
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempore ullam fugit soluta ipsam! Quod molestias saepe facilis dicta, quis mollitia, corporis ex est, debitis voluptates eaque inventore minus modi? Perspiciatis eligendi, vel necessitatibus, quia quibusdam modi incidunt distinctio ratione ea omnis dolorum labore. Quisquam nam blanditiis eligendi saepe fugit praesentium.</p>
                        </div>
                    </div>
                </div>
                <div class="media-object person-block" data-aos="zoom-in">
                    <div class="media-object-section thumb">
                        <img src="<?php echo JDC_ASSETS_URI ?>/img/person-4.jpg" alt="">
                    </div>
                    <div class="media-object-section text">
                        <h4 class="title">Armin Arlert</h4>
                        <h5 class="subtitle">Techincal Team Lead</h5>
                        <div class="description">
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempore ullam fugit soluta ipsam! Quod molestias saepe facilis dicta, quis mollitia, corporis ex est, debitis voluptates eaque inventore minus modi? Perspiciatis eligendi, vel necessitatibus, quia quibusdam modi incidunt distinctio ratione ea omnis dolorum labore. Quisquam nam blanditiis eligendi saepe fugit praesentium.</p>
                        </div>
                    </div>
                </div>
                <div class="media-object person-block" data-aos="zoom-in">
                    <div class="media-object-section thumb">
                        <img src="<?php echo JDC_ASSETS_URI ?>/img/person-5.jpg" alt="">
                    </div>
                    <div class="media-object-section text">
                        <h4 class="title">Sasha Braus</h4>
                        <h5 class="subtitle">HR Manager</h5>
                        <div class="description">
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempore ullam fugit soluta ipsam! Quod molestias saepe facilis dicta, quis mollitia, corporis ex est, debitis voluptates eaque inventore minus modi? Perspiciatis eligendi, vel necessitatibus, quia quibusdam modi incidunt distinctio ratione ea omnis dolorum labore. Quisquam nam blanditiis eligendi saepe fugit praesentium.</p>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>


<?php get_footer() ?>