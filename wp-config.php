<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'blenheims' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'P6D5P6If6Mg3s9Df939wSCH6ozWY2cmn8UVYP6VFVTVNSifNFj01KlW1a4oHMceY' );
define( 'SECURE_AUTH_KEY',  '1ZZGCQPYU56lyfadt0qw4NPkV6KwOEu2f1x955MBkQR4DuOzms0ocaztFoB63uZD' );
define( 'LOGGED_IN_KEY',    '6ok5dFp5zpIRkH1iu44qMlcZYddkgvCv0ExJ9f30IJfB9oKYg4dlTU8RqrY7fCAq' );
define( 'NONCE_KEY',        '0YjopQ0Wa5WMlWCL3x8Xn3YOPbTxf95fDOkONu8B8f7zewOgdODPHiYUV4IjL1Nz' );
define( 'AUTH_SALT',        'enDN1FJ3PpAi9osQ3qKR7AktRjmlHZbIxskqQEoav9u8JqLG8qGNo3xjG76kX9om' );
define( 'SECURE_AUTH_SALT', 'BlTkIIRunDSj0PltV2LKxWFD6oNpeC39AxcHLpKx67TmFaFQs26qFfmzmIF2h2c1' );
define( 'LOGGED_IN_SALT',   'ScvzR1x3zgTbGpcuH3ou8DZVZo4hZpDsPPkYIC3TYnTrgJdR4XUYZqyMsW6HHzVy' );
define( 'NONCE_SALT',       'ynkj2899e5Z272tqgFwpDNxRb8l637rIAGcniEYKp3aQGaNyLHiX24cUScdJWqRh' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
